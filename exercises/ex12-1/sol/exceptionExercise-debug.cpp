#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <stdexcept>

using std::cout;
using std::endl;

/* Collect integers from a file; store them in a vector */
std::vector<int> readFile(char* filename) {

  //report error if input file can't be read
  std::ifstream fin(filename);
  if (!fin.is_open()) {
    throw std::ios_base::failure("ERROR: File not found!");
  }

  //declare result vector with initial size 10
  std::vector<int> numbers(10);
  cout << "initial capacity: " << numbers.capacity() << endl;
  cout << "initial size: " << numbers.size() << endl;

  //attempt to fill the vector
  int n = 0;
  size_t index = 0;
  while(true) {
    fin >> n;
    if (fin.eof()) { //if we're out of values in file, return
      return numbers;
    }
    if (fin.fail()) { //if we failed to read an int, throw an exception
      throw std::invalid_argument("ERROR: File contains non-integer data!\n");
    }

    //otherwise, just add it into the vector
    try {
      std::cerr << "***adding n = " << n << std::endl;
      numbers.at(index) = n; //or could've used push_back which resizes for us!
    } catch (std::out_of_range& e) {
      //double the vector's capacity, then try again
      std::cerr << "CAUGHT old cap = " << numbers.capacity();
      numbers.reserve(2 * numbers.capacity());
      std::cerr << ", new cap = " << numbers.capacity() << std::endl;
      numbers.resize(numbers.capacity());
      std::cerr << ", new size = " << numbers.size() << std::endl;
      //numbers.at(index) = n;
      numbers[index] = n;
      std::cerr << "reached end of catch with index = " << index << std::endl;
    }
    index++;
  }

  throw std::logic_error("ERROR: should never get here!");

  return numbers;
}


int main(int argc, char **argv) {
  if (argc < 2) {
    cout << "ERROR: program requires a filename as an argument\n";
    return 0;
  }

  try {
    std::vector<int> numbers;
    numbers = readFile(argv[1]);

    cout << "Read numbers: ";
    for(int &i : numbers) {
      cout << i << " ";
    }
    cout << "\n";

  }

  catch (std::out_of_range& e) {
    cout << "ERROR: attempt to add to full vector\n" << endl;
  }

  catch (std::ios_base::failure& e) {
    cout << "Uh oh - bad file: " << e.what() << endl;
  }

  catch( const std::exception& ex) {
    cout << ex.what() << endl;
  }


  return 0;
}
