#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <stdexcept>

using std::cout;
using std::endl;

/* Collect integers from a file; store them in a vector */
std::vector<int> readFile(char* filename) {

  //report error if input file can't be read
  std::ifstream fin(filename);
  if (!fin.is_open()) {
    throw std::ios_base::failure("ERROR: File not found!");
  }

  //declare result vector with initial size 10
  std::vector<int> numbers;  // don't specify a starting size

  //attempt to fill the vector
  int n = 0;
  while(true) {
    fin >> n;
    if (fin.eof()) { //if we're out of values in file, return
      return numbers;
    }
    if (fin.fail()) { //if we failed to read an int, throw an exception
      throw std::invalid_argument("ERROR: File contains non-integer data!\n");
    }

    //otherwise, just add it into the vector
    try {
      numbers.push_back(n); 
    } catch (std::out_of_range& e) {
      throw e;  // will be caught in main, but shouldn't occur
    }
  }

  throw std::logic_error("ERROR: should never get here!");

  return numbers;
}


int main(int argc, char **argv) {
  if (argc < 2) {
    cout << "ERROR: program requires a filename as an argument\n";
    return 0;
  }

  try {
    std::vector<int> numbers;
    numbers = readFile(argv[1]);

    cout << "Read numbers: ";
    for(int &i : numbers) {
      cout << i << " ";
    }
    cout << "\n";

  }

  catch (std::out_of_range& e) {
    cout << "ERROR: attempt to add to full vector\n" << endl;
  }

  catch (std::ios_base::failure& e) {
    cout << "Uh oh - bad file: " << e.what() << endl;
  }

  catch( const std::exception& ex) {
    cout << ex.what() << endl;
  }


  return 0;
}
