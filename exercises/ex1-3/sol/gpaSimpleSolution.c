#include <stdio.h>

/* The purpose of this program is to compute GPAs for simple letter
   grades - no +/-, only A, B, C, D, F. Credits may be rational
   numbers.  Also determine and display notices for Dean's List
   (>=3.5) and Academic Probation (< 2.0).

SAMPLE RUN:

Welcome to the GPA program!
Enter grade and credits for each course below (Q to end):
course 1: A 4.0
course 2: b 2.7
course 3: B 3.5
course 4: c 3.0
course 5: f 1
course 6: a 3
course 7: Q
Your GPA is 3.06

PSEUDOCODE:

count gets 1
pointSum gets 0
creditSum gets 0
points gets 0
gpa gets 0

display “Welcome to GPA calculator”
prompt for grade & credits
display "course #", count

repeat while there is unread input
    read grade
    [convert grade to points]
    read credits
    add points * credits to pointSum
    add credits to creditSum
    add 1 to count
    display "course #", count

if creditSum > 0
   set gpa to pointSum / creditSum

display "GPA is ", gpa

if gpa >= 3.5
   display "Dean's List"
otherwise if count > 1 and gpa <= 2.0
   display "Uh-oh, Ac Pro..."

*/

int main(void) {

    char grade;
    int count = 1;
    float credits;
    double points;
    double totalpoints = 0, totalcredits = 0;
    double gpa = 0;

    printf("enter grades and credits, 'Q' to end\n");
    printf("course #%d: ", count);
    scanf(" %c", &grade);

    // repeat until end of input
    while (grade != 'Q' && grade != 'q') {

      switch (grade) {
      case 'a': case 'A': points = 4; break;
      case 'b': case 'B': points = 3; break;
      case 'c': case 'C': points = 2; break;
      case 'd': case 'D': points = 1; break;
      default: points = 0; break;
      }

      // read credits
      scanf(" %f", &credits);

      totalpoints += points * credits;
      totalcredits += credits;

      printf("grade is: %c  credits = %.1f  points = %3.1f \n", 
            grade, credits, points);

      count++;
      printf("course #%d: ", count);
      scanf(" %c", &grade);    // space before the %c means skip whitespace
    } // while not end

    if (totalcredits > 1) 
      gpa = totalpoints/totalcredits;

    printf("GPA is %.2f\n", gpa); 

    if (gpa >= 3.5) 
      printf("Dean's List!\n");
    else if (gpa <= 2.0)
      printf("uh-oh, Ac Pro...\n");

    return 0;
}
