#include <stdio.h>
#include <string.h>
#include <ctype.h>

/* The purpose of this program is to compute GPAs for full letter
   grades - including +/-. Conversion of grades to points is 
   A+, A == 4.0, A- == 3.7, B+ == 3.3, B == 3.0, B- == 2.7
   C+ == 2.3, C == 2, C- == 1.7, D+ == 1.3, D == 1, no D-, F == 0.

   Credits may be rational numbers.  Also determine and display
   notices for Dean's List (>=3.5) and Academic Probation (< 2.0).

SAMPLE RUN:

Welcome to the GPA program!
Enter grade and credits for each course below (^D to end):
course 1: A 4.0
course 2: b+ 2.7
course 3: B 3.5
course 4: c- 3.0
course 5: f 1
course 6: a+ 3
course 7: 
Your GPA is 3.05

PSEUDOCODE:

count gets 1
pointSum gets 0
creditSum gets 0
points gets 0
gpa gets 0

display “Welcome to GPA calculator”
prompt for grade & credits
display "course #", count

repeat while there is unread input
    read grade
    [convert grade to points]
    read credits
    add points * credits to pointSum
    add credits to creditSum
    add 1 to count
    display "course #", count

if creditSum > 0
   set gpa to pointSum / creditSum

display "GPA is ", gpa

if gpa >= 3.5
   display "Dean's List"
otherwise if count > 1 and gpa <= 2.0
   display "Uh-oh, Ac Pro..."

*/

int main(void) {

    char grade[3];  // extra one for null character at end
    int count = 1;
    float credits;
    double points;
    double totalpoints = 0, totalcredits = 0;
    double gpa = 0;

    printf("enter grades and credits, 'Q' to end\n");
    printf("course #%d: ", count);

    // repeat until end of input
    while (scanf("%s %f", grade, &credits) == 2) {

      switch (grade[0]) {
      case 'a': case 'A': points = 4; break;
      case 'b': case 'B': points = 3; break;
      case 'c': case 'C': points = 2; break;
      case 'd': case 'D': points = 1; break;
      default: points = 0; break;
      }

      grade[0] = toupper(grade[0]);
      if (strlen(grade) > 1) {
        if (grade[1] == '+' && grade[0] > 'A' && grade[0] <= 'D') {
          points += .3;
        }
        else if (grade[1] == '-' && grade[0] >= 'A' && grade[0] < 'D') {
          points -= .3;
        }
        else if (strcmp(grade, "A+") != 0) {
          printf("!!! invalid grade entered !!!\n");
        }
      }

      totalpoints += points * credits;
      totalcredits += credits;

      printf("grade is: %s  credits = %.1f  points = %3.1f \n", 
            grade, credits, points);

      count++;
      printf("course #%d: ", count);
    } // while not end

    if (totalcredits > 1) 
      gpa = totalpoints/totalcredits;

    printf("GPA is %.2f\n", gpa); 

    if (gpa >= 3.5) 
      printf("Dean's List!\n");
    else if (gpa <= 2.0)
      printf("uh-oh, Ac Pro...\n");

    return 0;
}
