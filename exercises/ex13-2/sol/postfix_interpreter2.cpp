#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <exception>
#include "stack.h"
#include "bounded_stack.h"

using std::cout;
using std::string;
using std::endl;
using std::cin;
using std::cout;
using std::istringstream;

int main() {
  Stack s;
  string exp;
  
  cout <<"enter a postfix expression: ";
  std::getline (cin, exp);
  istringstream exp_ss(exp); //put entire expression into stringstream
  string token;
  
  while (exp_ss >> token) {
    bool opertr = false;
    double d = 0;
    try {
      d = stod(token); //throws invalid_argument if cannot convert to double
      s.push(d);
    }
    catch (std::invalid_argument& e) {
      opertr = true;
    }
    
    
    if (opertr) {
      double o1;
      double o2;
      try {
	o1 = s.pop();
	o2 = s.pop();
      } catch (std::underflow_error& e) {
	cout <<"invalid expression!";
	return 0;
      }
      if (token == "*") {
	s.push(o2 * o1);
      }
      else if (token == "-") {
	s.push(o2 - o1);
      }
      else if (token == "+") {
	s.push(o2 + o1);
      }
      else if (token == "/") {
	if (o1 == 0) {
	  cout << "division by zero occured!";
	  return 0;
	}
	else {
	  s.push(o2 / o1);
	}
      }
      else {
	cout << "invalid expression!";
	return 0;
      }
    }
  }
  double r;
  try {
    r = s.pop();
  }
  catch (std::underflow_error& e) {
    cout <<"invalid expression!";
    return 0;
  }
  if (!s.isEmpty()) {
    cout <<"invalid expression!";
    return 0;
  }
    
  cout<<r<<endl;
  return 0;
}
