#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "read_line.h" // header file for functions we plan to test

void test_read_line();


/* 
 * Main function which just calls other tests and outputs an
 * "all tests pass" method at the end.
 */
int main() {

  // Call our (one and only) tester function
  test_read_line();

  // If we make it this far, then we didn't fail any asserts!
  printf("All tests passed!\n");  
  return 0;

}



/* 
 * This function uses asserts to test the read_line function
 * on different inputs.
 */
void test_read_line() {

  // Open a text file that contains 4 lines
  FILE * fp = fopen("lines.txt", "r");
  assert(fp);  // fail if input file didn't open
  
  // Read a short line ( <10 chars), a longer line, an empty line,
  // and a line that ends with EOF instead of \n
  char * result1 = read_line(fp);
  assert(!strcmp("short 1\n", result1));
  assert(result1[8] == 0);  //check that result1 ends with '\0'
  free(result1);
  result1 = 0;

  char * result2 = read_line(fp);
  assert(!strcmp("line 2 is longer than line 1 was!\n", result2));
  assert(result2[34] == 0);
  free(result2);
  result2 = 0;
  
  char * result3 = read_line(fp);
  assert(!strcmp("\n", result3));
  assert(result3[1] == 0);
  free(result3);
  result3 = 0;
  
  char * result4 = read_line(fp);
  assert(!strcmp("no newline at end", result4));
  assert(result4[17] == 0);
  free(result4);
  result4 = 0;
  
  fclose(fp);


  
  // Check that calling read_line on empty file returns NULL
  FILE * empty_file = fopen("empty.txt", "r");
  assert(fp);

  char * result5 = read_line(empty_file);
  assert(!result5);
  // No need to free this result since it should be NULL
  

  fclose(empty_file);
  
}
