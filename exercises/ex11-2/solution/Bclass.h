#include <iostream>
#include <string>

#ifndef _BCLASS_H
#define _BCLASS_H

#include "Aclass.h"

class B : public A {
private:
  int b;

public:
  B(int val = 0): A(), b(val) { };  // automatically sets a & d to 0 w/ A()
  B(int bval, int aval, double dval): A(aval, dval), b(bval) {
     d = 17; 
  };
  void setb(int val) { b = val; };
  virtual int fun() const override { return geta() * b * d; }
  virtual std::string toString() const override{ std::stringstream ss; ss << "[B: a = "; ss << geta(); ss << ", b = "; ss << b; ss << ", d = "; ss << d; ss << ", size = "; ss << sizeof(*this); ss << "]"; return ss.str(); };
};


#endif
