#ifndef LIST_H
#define LIST_H


typedef struct _node {
  char data;
  struct _node * next;
} Node;                           // Node is a linked list node that holds char data



Node * createNode(char);          // put char into a newly created node

void printList(const Node *);     // output the list iteratively
void printRec(const Node *);      // output the list recursively
void printReverse(const Node *);  // output the list in reverse

long length(const Node *);        // count and return the number of elements in the List
void clearList(Node **);          // get rid of (deallocate) entire list

void addAfter(Node *, char);      // add char immediately after existing node
void addFront(Node **, char);     // add char to beginning of list


// EXERCISE
char deleteAfter(Node *);         // delete node after current, return char
char deleteFront(Node **);        // delete first node, if any, return char


// EXERCISE
void removeAll(Node **, char);    // remove all occurrences of a particular character


// EXERCISE
// insert in order if not duplicate, assumes list is ordered!!
// return the address of the Node containing the inserted character
Node * insert(Node **, char);




// ---------- Other functions you may want to implement ----------

// insert char at the end of the list
void addTail(Node **, char); 
void addTailRec(Node **, char); // recursive version

// locate a value in the list, return pointer to containing node, or NULL
Node * find(Node *, char);

// delete char from list if it is there, return 1 success, 0 failure
int delete(Node **, char);   
int deleteRec(Node **, char);   // recursive version

// replace first occurrence of old with new, return 1 success, 0 failure
int replace(Node *, char old, char new); 


#endif

