#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

// put char into a newly created node
Node * createNode(char ch) {
  Node * node = (Node *) malloc(sizeof(Node));
  assert(node); //confirm malloc didn't fail

  node->data = ch;
  node->next = NULL;
  return node;
}

// output the list iteratively
void printList(const Node * cur) {
  while (cur != NULL) {
    printf("%c ", cur->data);
    cur = cur->next;  // advance to next node
  }
}

// output the list recursively
void printRec(const Node * head) {
  if (head != NULL) {
    printf("%c ", head->data);
    printRec(head->next);
  }
}

// output the list in reverse (recursive)
void printReverse(const Node * head) {
  if (head != NULL) {  // equivalent: if (head)
    printReverse(head->next);
    printf("%c ", head->data);
  }
}

// count and return the number of elements in the List (recursive)
long length(const Node * head) {
  if (head == NULL)
    return 0;
  return 1 + length(head->next);
}

// get rid of (deallocate) entire list, recursively from end to start
void clearList(Node **lptr) {
  if (*lptr != NULL) {
    clearList( &((*lptr)->next));
    free(*lptr);
    *lptr = NULL;
  }
}

// add char immediately after existing node
void addAfter(Node *node, char val) {
  if (node == NULL)
    return;
  Node *n = createNode(val);
  n->next = node->next;
  node->next = n;
}

// add char to beginning of list
void addFront(Node **lptr, char val) {
  Node *n = createNode(val);
  n->next = *lptr;
  *lptr = n;
}

// EXERCISE
// delete node after current, return char
char deleteAfter(Node *node) {

  // not yet implemented
  return '?';  // replace this stub

}

// EXERCISE
// delete first node, if any, return char
char deleteFront(Node **lptr) {

  // not yet implemented
  return '?';  // replace this stub

}

// EXERCISE
// remove all occurrences of a particular character
void removeAll(Node **lptr, char val) {

  // not yet implemented

}

// EXERCISE
// insert in order (assumes list is ordered)!!
// return the address of the node containing the character inserted
Node * insert(Node **lptr, char val) {

  // not yet implemented
  return NULL; // replace this stub

}



// ---------- Other functions you may want to implement ----------

// insert char at end of list
void addTail(Node **lptr, char val) {

  // not yet implemented

}


// insert char at end of list (recursive)
void addTailRec(Node **lptr, char val) {

  // not yet implemented

}

// find a value in the list, return pointer to containing node, or NULL
Node* find(Node * node, char val) {

  // not yet implemented
  return NULL;  // replace this stub

}

// delete 1st occurrence of char from list if there, return 1 success, 0 failure
int delete(Node **lptr, char val) {

  // not yet implemented
  return 0;

}

// recursive version of delete first occurrence
int deleteRec(Node **lptr, char val)  {

  // not yet implemented
  return 0;  // replace this stub

}

// replace first occurrence of old with new, return 1 success, 0 failure
int replace(Node * head, char old, char new) {

  // not yet implemented
  return 0;  // replace this stub

}

