#include <stdio.h>
#include <stdlib.h>
#include "soccer.h"


// TODO 1: IMPLEMENT update_player_stats HERE




  
void create_player (Player *p) {
  p->age = rand() % 18 + 18;
  p->jersey_num = rand() % 30 + 1;
  p->goalkeeper = (p->jersey_num - 1) == 0? 1: 0;
  p->Date.day = rand() % 30 + 1;
  p->Date.month = rand() % 12 + 1;
  p->Date.year = rand() % 9 + 2009;
  p->Stat.num_of_goals = p->goalkeeper == 1? 0 : rand() % 150 +1;
  p->Stat.num_of_assists = p->goalkeeper == 1? 0 : rand() % 150 +1;
  p->Stat.pass_accuracy = (float)rand()/(float)(RAND_MAX/100);
}

void create_team(Player team[], int size) {
  for (int i = 0; i < size; i++) {
    create_player(&team[i]);
  }
}


void print_team (Player team[], int size) {
for (int i = 0; i < size; i++) 
  printf("Player %d = [age: %d, jersey# = %d, goalie = %d, signed date = %d-%d-%d\n",
		 i, team[i].age, team[i].jersey_num, team[i].goalkeeper, 
		 team[i].Date.day, team[i].Date.month, team[i].Date.year);
 printf("Stats: goals = %d, assists = %d, pass accuracy = %0.1f]\n", 
		team[i].Stat.num_of_goals, team[i].Stat.num_of_assists, team[i].Stat.pass_accuracy); 
}

