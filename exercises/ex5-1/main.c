#include <stdio.h>
#include <string.h>
#include "soccer.h"



int main() {

  const int TEAMSIZE = 11;
  Player team[TEAMSIZE];
  Stats stat;

  create_team(team, TEAMSIZE);

  printf("Before Update:\n");
  print_team(team, TEAMSIZE);
  printf("Enter valid number of goals, number of assists, and pass accuracy:\n");

  //TODO 2: READ AND STORE STAT INFO TO stat 

  
  //TODO 3: FIND THE INDEX OF PLAYER IN THE ARRAY team WITH THE LATEST SIGNED DATE. 
  //IF THERE ARE MORE THAN ONE PLAYERS WITH THE SAME SIGNED DATE, SELECT THE ONE 
  //THAT APPEARS FIRST IN THE ARRAY.

  
  //TODO 4: CALL TO update_player_stats USING stat AND THE SELECTED PLAYER FROM THE ARRAY team
  
  printf("After Update:\n");
  print_team(team, TEAMSIZE);

  return 0;
}
