# this is a configuration file for bash shell


# The erase line below is an attempt to make backspace work in emacs via Windows
# Powershell, but this seems to break backspace on ugrad Unix command line via Mac Terminal.
# If we leave it commented out, backspace works in emacs via Powershell, but
# backspace on ugrad command line via Powershell breaks.  :(
# stty erase '^H'


# change this to vim if that is your preference
export EDITOR=emacs

# so we don't have to type as much
alias gccc='gcc -std=c99 -pedantic -Wall -Wextra'
alias g+++='g++ -std=c++11 -pedantic -Wall -Wextra'
